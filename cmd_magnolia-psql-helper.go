package main

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// CommandPather returns the path to a command.
type CommandPather interface {
	CommandPath() string
}

func main() {
	cmd := &cobra.Command{
		Use:           "magnolia-psql-helper",
		Short:         "magnolia-psql-helper helps fixing errors in older version of the magnolia-helm chart with postgresql.",
		SilenceErrors: true,
	}
	cmd.AddCommand(
		newCompletion(cmd),
		newVersion(cmd),
		newFixupPgdata(cmd),
	)
	logrus.Infof("running version %s", version)
	if err := cmd.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		os.Exit(1)
	}
}
