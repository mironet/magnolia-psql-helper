package main

import (
	"bytes"
	"fmt"
	"os"
	"strings"
	"testing"
)

func Test_tree(t *testing.T) {
	// Prepare directory structure.
	const dirs = "schni/schna/schnappi/das/kleine/krokodil"
	const wantTree = `
└──schni
   └──schna
      └──schnappi
         └──das
            └──kleine
               └──krokodil`

	tmpDir, err := prepareTestDirTree(dirs)
	if err != nil {
		t.Error(err)
		return
	}
	defer os.RemoveAll(tmpDir)

	type wantFunc func(*TreeNode) error

	type args struct {
		start string
	}
	tests := []struct {
		name    string
		args    args
		want    wantFunc
		wantErr bool
	}{
		{
			name: "walk",
			args: args{start: tmpDir},
			want: func(tn *TreeNode) error {
				var buf bytes.Buffer
				_ = tn.Print(&buf, "")
				if strings.HasSuffix(strings.TrimSpace(buf.String()), strings.TrimSpace(wantTree)) {
					return nil
				}
				return fmt.Errorf("output %s does not match %s", buf.String(), wantTree)
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := dirtree(tt.args.start)
			if (err != nil) != tt.wantErr {
				t.Errorf("tree() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err := got.Print(os.Stderr, ""); err != nil {
				t.Error(err)
				return
			}
			if tt.want != nil {
				if err := tt.want(got); err != nil {
					t.Error(err)
					return
				}
			}
		})
	}
}
