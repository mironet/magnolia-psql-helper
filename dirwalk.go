package main

import (
	"fmt"
	"os"
	"path/filepath"
)

// dirtree recursively walks the start directory listing its contents.
func dirtree(start string) (*TreeNode, error) {
	node := new(TreeNode)
	node.Payload = filepath.Base(start)

	dir, err := os.Open(start)
	if err != nil {
		return nil, err
	}
	defer dir.Close()

	list, err := dir.Readdir(0)
	if err != nil {
		return nil, fmt.Errorf("error listing dir contents: %w", err)
	}

	for _, v := range list {
		if v.IsDir() {
			// Recurse.
			child, err := dirtree(filepath.Join(start, v.Name()))
			if err != nil {
				return nil, err
			}
			node.Add(child)
			continue
		}
		node.Add(&TreeNode{Payload: v.Name()})
	}

	return node, nil
}
