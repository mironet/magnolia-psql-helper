# Magnolia PostgreSQL fixup tool

This is an internal migration helper to fix earlier errors in deployments of the magnolia-helm chart.
It has no specific use outside of this context.
