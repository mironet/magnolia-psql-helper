package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

func newFixupPgdata(pather CommandPather) *cobra.Command {
	var flags struct {
		source  []string
		target  []string
		ownerID string
		marker  string
	}

	var cmd = &cobra.Command{
		Use:     "fixup-pgdata",
		Short:   "fixup-pgdata moves data files into the correct directories.",
		Example: fmt.Sprintf("  %[1]s fixup-pgdata --sample", pather.CommandPath()),
		RunE: func(cmd *cobra.Command, args []string) error {
			if flags.source == nil {
				return fmt.Errorf("at least one --source is mandatory")
			}
			if flags.target == nil {
				return fmt.Errorf("at least one --target is mandatory")
			}
			if len(flags.source) != len(flags.target) {
				return fmt.Errorf("number of --sources and --targets doesn't match (source = %d, target = %d)", len(flags.source), len(flags.target))
			}
			// Add basic sanity checks, where the usage help message should be
			// printed on error, before this line. After this line, the usage
			// message is no longer printed on error.
			cmd.SilenceUsage = true

			for k := range flags.source {
				if err := fixupPgdata(flags.source[k], flags.target[k], flags.ownerID, flags.marker); err != nil {
					return err
				}
			}

			return nil
		},
	}

	cmd.Flags().StringSliceVar(&flags.source, "source", nil, "where current data volume is mounted on")
	cmd.Flags().StringSliceVar(&flags.target, "target", nil, "where to move the data to")
	cmd.Flags().StringVar(&flags.marker, "marker", "PG_VERSION", "marker file to check for presence before moving anything")
	cmd.Flags().StringVar(&flags.ownerID, "owner", "70", "The owner id of the user whe assign to the target directory")
	return cmd
}
