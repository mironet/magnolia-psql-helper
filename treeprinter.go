package main

import (
	"encoding/json"
	"fmt"
	"io"
	"sort"
	"sync"
)

// TreeNode is a node in the tree.
type TreeNode struct {
	Payload interface{}

	children []*TreeNode
	mux      sync.Mutex
}

func (n *TreeNode) String() string {
	if n == nil {
		return ""
	}
	if n.Payload == nil {
		return "<nil>"
	}

	switch s := n.Payload.(type) {
	case interface{ ShortName() string }:
		return s.ShortName()
	case fmt.Stringer:
		return s.String()
	case string:
		return s
	}

	return "?"
}

// Add another node to this node.
func (n *TreeNode) Add(other *TreeNode) {
	n.mux.Lock()
	defer n.mux.Unlock()
	for _, v := range n.children {
		if v.String() == other.String() {
			// Merge nodes.
			v.Payload = other.Payload
			v.children = append(v.children, other.children...)
			return
		}
	}
	n.children = append(n.children, other)
}

// MarshalJSON .
func (n *TreeNode) MarshalJSON() ([]byte, error) {
	type str struct {
		Name     string
		Children []*TreeNode
		Payload  interface{}
	}

	var name = n.String()
	out := str{
		Name:     name,
		Children: n.children,
		Payload:  n.Payload,
	}
	if s, ok := n.Payload.(interface{ ShortName() string }); ok {
		out.Name = s.ShortName()
	}

	return json.Marshal(out)
}

const (
	nodeSymbol  = "├──"
	edgeSymbol  = "└──"
	trunkSymbol = "│  "
	spaceSymbol = "   "
)

// Print the tree in a tree-like textual representation.
func (n *TreeNode) Print(out io.Writer, prefix string) error {
	n.mux.Lock()
	defer n.mux.Unlock()

	if n == nil {
		return nil
	}
	// First print self.
	if _, err := fmt.Fprintln(out, n.String()); err != nil {
		return err
	}

	// Try to sort.
	sort.Slice(n.children, func(i, j int) bool {
		a := n.children
		return a[i].String() < a[j].String()
	})

	// Then walk children slice.
	for i, node := range n.children {
		trunk := trunkSymbol // Prepend this.
		if i == len(n.children)-1 {
			// No more children, this is an edge.
			if _, err := fmt.Fprintf(out, prefix+edgeSymbol); err != nil {
				return err
			}
			trunk = spaceSymbol
		} else {
			// More siblings.
			if _, err := fmt.Fprintf(out, prefix+nodeSymbol); err != nil {
				return err
			}
		}

		// Recurse.
		if err := node.Print(out, prefix+trunk); err != nil {
			return err
		}
	}

	return nil
}
