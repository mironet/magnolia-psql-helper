module gitlab.com/mironet/magnolia-psql-helper

go 1.15

require (
	github.com/go-test/deep v1.0.7
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.1.3
)
