package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

// https://golang.org/pkg/path/filepath/#Walk
func prepareTestDirTree(tree string) (string, error) {
	tmpDir, err := ioutil.TempDir("", "")
	if err != nil {
		return "", fmt.Errorf("error creating temp directory: %v", err)
	}

	err = os.MkdirAll(filepath.Join(tmpDir, tree), 0755)
	if err != nil {
		os.RemoveAll(tmpDir)
		return "", err
	}

	return tmpDir, nil
}

func Test_fixupPgdata(t *testing.T) {
	const marker = "PG_VERSION"

	// Create test directories and files.
	tmpDir, err := prepareTestDirTree("source")
	if err != nil {
		fmt.Printf("unable to create test dir tree: %v\n", err)
		return
	}
	defer os.RemoveAll(tmpDir)

	// Create an additional directory.
	if err := os.Mkdir(filepath.Join(tmpDir, "source", "other"), 0755); err != nil {
		t.Error(err)
		return
	}

	// Create a few files in the base dir ...
	for i := 0; i < 10; i++ {
		if err := ioutil.WriteFile(filepath.Join(tmpDir, "source", fmt.Sprintf("testfile-%d.txt", i)), []byte("Testing123"), 0644); err != nil {
			t.Error(err)
			return
		}
		// ... and in the "other" dir.
		if err := ioutil.WriteFile(filepath.Join(tmpDir, "source", "other", fmt.Sprintf("testfile-%d.txt", i)), []byte("Testing123"), 0644); err != nil {
			t.Error(err)
			return
		}
	}
	// And now create the marker file.
	if err := ioutil.WriteFile(filepath.Join(tmpDir, "source", marker), []byte("11"), 0600); err != nil {
		t.Error(err)
		return
	}

	type args struct {
		source string
		target string
		owner  string
		marker string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "simple move",
			args: args{
				source: filepath.Join(tmpDir, "source"),
				target: filepath.Join(tmpDir, "source", "target/subdir"),
				owner:  "1000",
				marker: marker,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tree, err := dirtree(tt.args.source)
			if err != nil {
				t.Error(err)
				return
			}
			var before bytes.Buffer
			tree.Print(&before, "before: ")
			if err := fixupPgdata(tt.args.source, tt.args.target, tt.args.owner, tt.args.marker); (err != nil) != tt.wantErr {
				t.Errorf("fixupPgdata() error = %v, wantErr %v", err, tt.wantErr)
			}

			tree, err = dirtree(tt.args.source)
			if err != nil {
				t.Error(err)
				return
			}
			var after bytes.Buffer
			tree.Print(&after, "after: ")

			t.Log(before.String())
			t.Log(after.String())
			// TODO: Write a better test to check the final dir structure.
		})
	}
}
