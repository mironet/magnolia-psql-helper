package main

import (
	"errors"
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"strconv"

	"github.com/sirupsen/logrus"
)

// Move stuff from source dir to target dir if (and only if) target does not
// exist.
func fixupPgdata(source, target, owner, marker string) error {
	// Check if the source data dir exists.
	_, err := os.Stat(source)
	if err != nil && os.IsNotExist(err) {
		logrus.Infof("source directory %s does not exist, skipping", source)
		return nil
	}
	if err != nil {
		return fmt.Errorf("error while checking source directory: %w", err)
	}

	// Check if the marker file exists on source.
	_, err = os.Stat(filepath.Join(source, marker))
	if err != nil && os.IsNotExist(err) {
		logrus.Infof("source directory does not contain marker file %s, skipping", marker)
		return nil
	}
	if err != nil {
		return fmt.Errorf("error checking marker file %s: %w", marker, err)
	}

	// Check if the destination data dir exists.
	_, err = os.Stat(target)
	if err == nil {
		logrus.Infof("target directory already exists, not moving anything")
		return nil
	}
	if !os.IsNotExist(err) {
		return fmt.Errorf("error checking if target directory exists, check for permission errors: %w", err)
	}

	// List directory entries now, before creating the target directory.
	dir, err := os.Open(source)
	if err != nil {
		return err
	}
	defer dir.Close()
	names, err := dir.Readdirnames(0)
	if err != nil {
		return fmt.Errorf("error listing source directory")
	}
	// Print dir tree structure before changes.
	tree, err := dirtree(source)
	if err != nil {
		return fmt.Errorf("error printing dir structure: %w", err)
	}
	if err := tree.Print(os.Stderr, "before: "); err != nil {
		return fmt.Errorf("error printing dir structure: %w", err)
	}

	// From here on the directory should not exist, let's try to create it.
	if err := os.MkdirAll(target, 0700); err != nil {
		return err
	}

	uidstr, gidstr, err := lookupOwner(owner)
	if err != nil {
		return fmt.Errorf("error looking up owner: %w", err)
	}
	uid, err := strconv.Atoi(uidstr)
	if err != nil {
		return fmt.Errorf("error converting uid to int: %w", err)
	}
	gid, err := strconv.Atoi(gidstr)
	if err != nil {
		return fmt.Errorf("error converting gid to int: %w", err)
	}

	// Change ownership of the target.
	if err := os.Chown(target, uid, gid); err != nil {
		return err
	}

	// Now let's move everything except the just created directory to the
	// target.
	for _, v := range names {
		logrus.Infof("moving %s to %s", filepath.Join(source, v), filepath.Join(target, v))
		if err := os.Rename(filepath.Join(source, v), filepath.Join(target, v)); err != nil {
			return fmt.Errorf("error moving file/dir %s: %w", v, err)
		}
	}

	// Print dir tree structure after changes.
	tree, err = dirtree(source)
	if err != nil {
		return fmt.Errorf("error printing dir structure: %w", err)
	}
	if err := tree.Print(os.Stderr, "after: "); err != nil {
		return fmt.Errorf("error printing dir structure: %w", err)
	}

	return nil
}

func lookupOwner(owner string) (uid, gid string, err error) {
	// Find out user id by name or id.
	usr, err := user.Lookup(owner)
	var e user.UnknownUserError
	if errors.As(err, &e) {
		usr, err := user.LookupId(owner)
		if err != nil {
			return owner, owner, nil // We try this in case the user is not present on the current system.
		}
		return usr.Uid, usr.Gid, nil
	}
	if err != nil {
		return "", "", err
	}
	return usr.Uid, usr.Gid, nil
}
