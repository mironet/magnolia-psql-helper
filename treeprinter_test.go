package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"math/rand"
	"testing"

	"github.com/go-test/deep"
)

const (
	chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
)

func fixture(t *testing.T, filename string) []byte {
	bb, err := ioutil.ReadFile(fmt.Sprintf("testdata/fixtures/%s", filename))
	if err != nil {
		t.Fatal(err)
	}
	return bb
}

type randStringer struct {
	min, max int
}

func (r randStringer) String() string {
	n := r.min + rand.Intn(r.max-r.min)
	var str = make([]byte, n)
	for i := 0; i < n; i++ {
		str[i] = chars[rand.Intn(len(chars))]
	}
	return string(str)
}

// Generate a random tree, having a random number of children on each level up
// to 'level' deep.
func randomTree(level int, maxChildren int) *TreeNode {
	level--
	node := &TreeNode{
		Payload: randStringer{min: 5, max: 20},
	}

	if level == 0 {
		return node
	}

	for i := 0; i < maxChildren; i++ {
		node.children = append(node.children, randomTree(level, maxChildren))
	}

	return node
}

func TestTreeNode_Print(t *testing.T) {
	rand.Seed(42)
	tree := randomTree(3, 10)
	bb, err := ioutil.ReadFile("testdata/tree_output.txt")
	if err != nil {
		t.Fatal(err)
	}
	wantOut := string(bb)

	type args struct {
		prefix string
	}
	tests := []struct {
		name    string
		node    *TreeNode
		args    args
		wantOut string
	}{
		{
			name: "simple tree",
			node: tree,
			args: args{
				prefix: "",
			},
			wantOut: wantOut,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := tt.node
			out := &bytes.Buffer{}
			n.Print(out, tt.args.prefix)
			gotOut := out.String()
			if diff := deep.Equal(gotOut, tt.wantOut); len(diff) > 0 {
				t.Error(diff)
			}
			t.Log(gotOut)
		})
	}
}

func TestTreeNode_EqualChildren(t *testing.T) {
	bb := fixture(t, "tree_equal_children.txt")
	tree := &TreeNode{Payload: "root"}
	schni := &TreeNode{Payload: "schni"}
	schni.Add(&TreeNode{Payload: "schnu1"})
	schni.Add(&TreeNode{Payload: "schnu2"})
	schni.Add(&TreeNode{Payload: "schnu3"})
	tree.Add(schni)

	tree.Add(&TreeNode{
		Payload: "schna",
	})

	schni2 := &TreeNode{Payload: "schni"}
	schni2.Add(&TreeNode{Payload: "schne1"})
	schni2.Add(&TreeNode{Payload: "schne2"})

	tree.Add(schni2)

	type args struct {
		prefix string
	}
	tests := []struct {
		name    string
		node    *TreeNode
		args    args
		wantOut string
	}{
		{
			name: "simple tree",
			node: tree,
			args: args{
				prefix: "",
			},
			wantOut: string(bb),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := tt.node
			out := &bytes.Buffer{}
			n.Print(out, tt.args.prefix)
			gotOut := out.String()
			if diff := deep.Equal(gotOut, tt.wantOut); len(diff) > 0 {
				t.Error(diff)
			}
			t.Log(gotOut)
		})
	}
}
